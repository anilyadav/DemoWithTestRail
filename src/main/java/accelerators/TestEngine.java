package accelerators;


import TestRail.APIClient;
import TestRail.APIException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.http.auth.AuthenticationException;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.xml.sax.SAXException;
import utils.MyListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author HarishDaggupati
 *
 */

public class TestEngine {
    public static final Logger LOG = Logger.getLogger(TestEngine.class);
    public WebDriver WebDriver = null;
    public EventFiringWebDriver Driver=null;
    public String browser = null;
    public String baseUrl = null;
    public ExtentReports extent ;
    public ExtentTest logger;
    DesiredCapabilities capabilities;
    public LinkedHashMap<String,Integer> statusMap=new LinkedHashMap<String,Integer>();
    public String htmlFile="index.html";
    public String extentReportDirectoryPath = System.getProperty("user.dir")+"\\Reports\\"+htmlFile;
    public String userEmail;
    public String pwd;
    public String orderReferenceNumber;
    APIClient client = new APIClient(" https://swapnilg.testrail.io/");
    public String statusMapFilePath = System.getProperty("user.dir")+"\\statusMap.properties";
    public LinkedHashMap<String,Integer> tRMap=new LinkedHashMap<String,Integer>();
    public LinkedHashMap<String,String> tcIdAndScenario=new LinkedHashMap<String,String>();
    public  LinkedHashMap<String,Integer> executionMap=new LinkedHashMap<String,Integer>();
    public String tcId;
    ArrayList<String> failedSummaryList=new ArrayList<>();
    LinkedHashMap<String,String> summaryAndStatusFromJira=new LinkedHashMap<>();
    String summary;
    String issueStatus;
    ArrayList<String> incrementIssueCount=new ArrayList<>();
    String auth= new String(com.sun.jersey.core.util.Base64.encode("anil01.yadav@gmail.com" + ":" + "password@04"));
    Client clientQuery = Client.create();

    @BeforeSuite(alwaysRun=true)
    public void beforeSuite() throws Throwable{
        //loadConfig();
        extent = new ExtentReports(extentReportDirectoryPath, true);
    }

    @BeforeMethod(alwaysRun=true)
    @Parameters({"browser","baseUrl"})
    public void beforeMethod(String browser,String baseUrl) throws IOException, InterruptedException
    {

		/*get configuration */
        this.browser = browser;
        this.baseUrl = baseUrl;
        this.setWebDriverForLocal(browser);
        this.Driver = new EventFiringWebDriver(this.WebDriver);
        MyListener myListener = new MyListener();
        this.Driver.register(myListener);
        Driver.manage().window().maximize();
        Driver.manage().deleteAllCookies();
        Driver.get(baseUrl);
        Driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

    }

    @Parameters({"browser"})
    @AfterMethod(alwaysRun=true)
    public void afterMethod(ITestResult result,String browser) throws IOException, AuthenticationException {
        if (result.getStatus() == ITestResult.FAILURE) {
            statusMap.put(tcId, result.getStatus());//Prepares test cases and its status to upload to testrail
            logger.log(LogStatus.FAIL, "Snapshot below: " + logger.addScreenCapture(capture(Driver, "screenShot")));
            logFile(tcId, result.getThrowable().toString());
            createDefectIfNotExist("EC"," Test case number:"+tcId+"is failed","Bug");
        } else if (result.getStatus() == ITestResult.SKIP) {
            statusMap.put(tcId, result.getStatus());//Prepares test cases and its status to upload to testrail
            logger.log(LogStatus.SKIP, "C" + tcId + " : This Scenario must be passed in the previous environment");
        } else if (result.getStatus() == ITestResult.SUCCESS) {
            statusMap.put(tcId, result.getStatus());//Prepares test cases and its status to upload to testrail
        }
        if (browser.equalsIgnoreCase("firefox")) {
            Driver.quit();
        }
        else{
            Driver.quit();
        }
    }
    @AfterSuite
    public void afterSuite() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException, APIException {
        updateStatusMapFile(statusMapFilePath);
        updateTR();
        extent.endTest(logger);
        extent.flush();
//        extent.close();
    }

    private void setWebDriverForLocal(String browser) throws IOException, InterruptedException
    {
        switch(browser)
        {
            case "firefox":
                System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\Drivers\\geckodriver.exe");
                capabilities = new DesiredCapabilities();
                capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
                this.WebDriver = new FirefoxDriver(capabilities);
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");
                capabilities = DesiredCapabilities.chrome();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("test-type");
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                this.WebDriver = new ChromeDriver(capabilities);
                break;

        }

    }

    protected  String capture(String screenShotName) throws IOException
    {
        return capture(this.Driver,screenShotName);
    }

    private   String capture(WebDriver driver,String screenShotName) throws IOException
    {
        String ss=String.format(getCurrentTimeStamp(),screenShotName)+".png";
        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String dest = System.getProperty("user.dir") +"/Reports/"+ss;
        File destination = new File(dest);
        FileUtils.copyFile(source, destination);
//        return "../ErrorScreenshots/"+screenShotName+".png";
        return ss;
    }

    private void loadConfig() {

        Properties prop = new Properties();
        InputStream input = null;
        try
        {
            input = new FileInputStream(System.getProperty("user.dir")+"/config.properties");
            prop.load(input);
            for (final Map.Entry<Object, Object> entry : prop.entrySet()) {
                System.setProperty((String) entry.getKey(), (String) entry.getValue());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String getCurrentTimeStamp() {
        return new SimpleDateFormat("'%s'MMddHHmmssSSS").format(new Date());
    }

    private  void logFile(String tcId,String consoleOp) {
        try
        {
            String filename= System.getProperty("user.dir")+"\\Reports\\"+"logFile.txt";
            FileWriter fw = new FileWriter(filename,true); //the true will append the new data
            fw.write(tcId+" : ");//appends the string to the file
            fw.write(consoleOp+"\n");//appends the string to the file
            fw.close();
        }
        catch(IOException ioe)
        {
            System.err.println("IOException: " + ioe.getMessage());
        }

    }
    private String getCurrentTimeStampForLogFie() {
        return new SimpleDateFormat("MM-dd-yyy").format(new Date());
    }

    public  void updateTR() throws IOException, APIException, XPathExpressionException, ParserConfigurationException, SAXException {

        String tRTestRun=System.getenv("TestRun");
        client.setUser("swapnil.gore@cigniti.com");
        client.setPassword("Cigniti@123");
        BufferedReader br = new BufferedReader(new FileReader(statusMapFilePath));
        String sCurrentLine;
        while ((sCurrentLine = br.readLine()) != null) {
            String[] temp = sCurrentLine.split(":");
            String tcId=temp[0];
            String trId=tcId.trim().substring(0,tcId.length());
            tRMap.put(trId, mapStatus(Integer.parseInt(temp[1])));
        }
        br.close();
        for(Map.Entry<String,Integer> entry:tRMap.entrySet() ){
            Map data = new HashMap();
            data.put("status_id", entry.getValue());
            data.put("custom_result_environment", 3);
            data.put("comment", "Test results were updated through automation");
            JSONObject r = (JSONObject) client.sendPost("add_result_for_case/"+"1"+"/" + entry.getKey(), data);
        }
        System.out.println("testRailTestRun===="+"R1");
        System.out.println("tRMap======="+tRMap);
        System.out.println("stageBuildNum======="+System.getenv("stageBuildNum"));
    }
    private int mapStatus(int statusCode){
        int status;
        switch (statusCode){
            case 1:
                status=1;
                break;
            case 2:
                status=5;
                break;
            default:
                status=5;
        }
        return status;
    }

    public  void updateStatusMapFile(String file) {
        try {
            BufferedReader br1 = new BufferedReader(new FileReader(file));
            BufferedReader br2 = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            if (br1.readLine() != null) {
                br1.close();
                while ((sCurrentLine = br2.readLine()) != null) {
                    String[] temp = sCurrentLine.split(":");
                    executionMap.put(temp[0], Integer.parseInt(temp[1]));
                }
                br2.close();
                executionMap.putAll(statusMap);
                FileWriter fw = new FileWriter(file);
                for (Map.Entry<String, Integer> entry : executionMap.entrySet()) {
                    fw.write(entry.getKey() + ":" + entry.getValue());
                    fw.write("\n");
                }
                fw.close();
            } else {

                FileWriter fw = new FileWriter(file, true); //the true will append the new data
                for (Map.Entry<String, Integer> entry : statusMap.entrySet()) {
                    fw.write(entry.getKey() + ":" + entry.getValue());
                    fw.write("\n");
                }
                fw.close();
            }

        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

    private void createDefectIfNotExist(String projectKey,String summary,String issueType)throws AuthenticationException, ClientHandlerException, IOException{

        String encodedJql = URLEncoder.encode("project=\""+projectKey+"\" AND text ~ \""+summary+"\"", "UTF-8");
        WebResource webResourceCreate = clientQuery.resource("https://demomanagement.atlassian.net/rest/api/2/search?jql="+encodedJql);
        ClientResponse responseCreate = webResourceCreate.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").get(ClientResponse.class);
        String jsonResponse = responseCreate.getEntity(String.class);
        //parse the Jira Response of Json file and gives summary of issues
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonResponse);
            int totalCount= Integer.parseInt(jsonObject.get("total").toString());
            if(totalCount==0){
                createDefect(projectKey,summary,issueType);
            }
            else if(totalCount!=0){
                JSONArray issues= (JSONArray) jsonObject.get("issues");
                if(issues.size()==1){
                    Iterator i = issues.iterator();
                    // take each value from the json array separately
                    while (i.hasNext()) {
                        JSONObject innerObj = (JSONObject) i.next();
                        updateDefect(innerObj.get("key").toString());
                    }
                }

            }
        }  catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * purpose: Creates an  issue in Jira
     * @param projectKey
     * @param summary
     * @param issueType
     * @throws AuthenticationException
     * @throws ClientHandlerException
     * @throws IOException
     */

    private void createDefect(String projectKey,String summary,String issueType)throws AuthenticationException, ClientHandlerException, IOException{

        WebResource webResourceCreate = clientQuery.resource("https://demomanagement.atlassian.net/rest/api/latest/issue");
        String data = "{\"fields\"" +":{\"project\"" + ":{\"key\":\""+projectKey+"\"},"+"\"summary\":\""+summary+" \",\"issuetype\":{\"name\":\""+issueType+"\"}}}";
        ClientResponse responseCreate = webResourceCreate.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").post(ClientResponse.class, data);
        int statusCode = responseCreate.getStatus();
        if (statusCode == 401) {
            throw new AuthenticationException("Invalid Username or Password");
        } else if (statusCode == 403) {
            throw new AuthenticationException("Forbidden");
        } else if (statusCode == 200 || statusCode == 201) {
            System.out.println("Ticket Create succesfully");
        } else if (statusCode==400) {
            System.out.println("Bad Request");
        } else {
            System.out.print("Http Error : " + statusCode);
        }
    }

    private  void updateDefect(String key)throws AuthenticationException, ClientHandlerException, IOException{

        WebResource webResourceCreate = clientQuery.resource("https://demomanagement.atlassian.net/rest/api/2/issue/"+key+"/transitions?expand=transitions.fields");
        String data = "{\"transition\": {\"id\": \"11\"}}";
        ClientResponse responseCreate = webResourceCreate.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").post(ClientResponse.class, data);

    }
}
