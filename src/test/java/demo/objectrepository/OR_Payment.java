package demo.objectrepository;

/**
 * Created by E002465 on 12-06-2017.
 */
public interface OR_Payment {
    String paymentMode = "xpath=//a[contains(text(),'%s')]";
    String confirmMyOrderBtn="xpath=//span[text()='I confirm my order']";
    String orderConfirmLabel="css=.cheque-indent>strong";
    String completeOrderDetails="css=div.box";


}
