package demo.objectrepository;

/**
 * Created by E002465 on 12-06-2017.
 */
public interface OR_AddToCart {
    String product = "css=div.right-block a.product-name";
    String searchBarTxt = "id=search_query_top";
    String submitSearchBtn = "name=submit_search";
    String addTocartBtn = "css=#add_to_cart button";
    String proceedToCheckOutBtn = "css=a[title='Proceed to checkout']";
    String proceedToCheckOutinSummaryBtn = "css=p.cart_navigation.clearfix a[title='Proceed to checkout']";
    String proceedToCheckOutinAddressBtn = "name=processAddress";
    String proceedToCheckOutinCarrierBtn = "name=processCarrier";
    String agreeToTAndCChkBox = "id=cgv";

   /* String productName = "xpath=(//a[@title='Faded Short Sleeve T-shirts' and @class='product-name'])[1]";
    String addToCartBtn = "xpath=(//span[contains(text(),'$16.51')]/../..//div[@class='button-container']/a/span[text()='Add to cart'])[1]";
    String proceedToCheckoutBtn ="xpath=//a[@title='Proceed to checkout']/span";
    String summaryProceedToCheckoutBtn="xpath=//p[@class='cart_navigation clearfix']//a[@title='Proceed to checkout']/span";
    String usernameTxt ="xpath=//input[@id='email']";
    String passwordTxt ="xpath=//input[@id='passwd']";
    String signInBtn="xpath=//button[@id='SubmitLogin']/span";
    String addressProceedToCheckoutBtn="xpath=//button[@name='processAddress']/span";
    String shippingProceedToCheckoutBtn= "xpath=//input[@id='cgv']";
    String payByCheckBtn="xpath=//a[@title='Pay by check.']";
    String confirmMyOrderBtn="xpath=//span[text()='I confirm my order']";
*/
}
