package demo.objectrepository;

/**
 * Created by E002465 on 12-06-2017.
 */
public interface OR_CreateAccount {
    String createAccEmailAddressTxt = "id=email_create";
    String submitCreateBtn = "id=SubmitCreate";
    String genderMaleradioBtn = "id=id_gender1";
    String genderFemaleRadioBtn = "id=id_gender2";
    String firstNameTxt = "id=customer_firstname";
    String lastNameTxt = "id=customer_lastname";
    String passwdTxt = "id=passwd";
    String daysDropDown = "id=days";
    String monthsDropDown = "id=months";
    String yearsDropDown = "id=years";
    String address_firstNameTxt = "id=firstname";
    String address_lastNameTxt = "id=lastname";
    String address_companyTxt = "id=company";
    String address_address1Txt = "id=address1";
    String address_address2Txt = "id=address2";
    String address_cityTxt = "id=city";
    String address_stateDropDown = "id=id_state";
    String address_postcodeTxt = "id=postcode";
    String address_mobilePhoneTxt = "id=phone_mobile";
    String address_aliasTxt = "id=alias";
    String address_submitAccountBtn = "id=submitAccount";
    String logOutBtn = "className=logout";

}
