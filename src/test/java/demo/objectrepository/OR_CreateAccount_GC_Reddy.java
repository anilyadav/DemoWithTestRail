package demo.objectrepository;

/**
 * Created by E002465 on 12-06-2017.
 */
public interface OR_CreateAccount_GC_Reddy {
    String myAccBtnBtn = "id=tdb3";
    String welcomeHeadre = "css=#bodyContent>h1";
    String createAccountContinueBtn = "id=tdb4";
    String maleRadiobtn = "css=[value='m']";
    String feMaleRadiobtn = "css=[value='f']";
    String firstNameTxt = "name=firstname";
    String lasttNameTxt = "name=lastname";
    String dobTxt = "id=dob";
    String emailAddressTxt = "name=email_address";
    String companyTxt = "name=company";
    String streetAddressTxt = "name=street_address";
    String countryTxt = "name=country";
    String postCodeTxt = "name=postcode";
    String cityTxt = "name=city";
    String stateTxt = "name=state";
    String telephoneTxt = "name=telephone";
    String faxTxt = "name=fax";
    String newsLetterTxt = "name=newsletter";
    String passwordTxt = "name=password";
    String confirmPasswordTxt = "name=confirmation";
    String continueBtn = "id=tdb4";
    String accConfirmLabel = "css=#bodyContent>h1";

}
