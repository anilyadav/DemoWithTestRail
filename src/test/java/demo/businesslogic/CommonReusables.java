package demo.businesslogic;

import accelerators.ActionEngine;
import com.relevantcodes.extentreports.LogStatus;
import demo.objectrepository.*;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;

import static demo.objectrepository.OR_AddToCart.product;

/**
 * Created by E002465 on 06-06-2017.
 */
public class CommonReusables extends ActionEngine implements OR_CreateAccount,OR_Signin,OR_AddToCart,OR_Payment,OR_OrderHistory {

    protected void selectGender(String gender) throws Throwable {
        waitForElementToBeClickable(genderMaleradioBtn);
        if(gender.toLowerCase().contains("male")){
            click(genderMaleradioBtn,"Male radio button");
        }
        else if(gender.toLowerCase().contains("female")){
            click(genderFemaleRadioBtn,"Female radio button");
        }
    }
    protected void selectDob(String dOB){
        try {
            String[] dateOfBirth = dOB.split("/");
            String day = dateOfBirth[0].replaceFirst("0","");
            String month = dateOfBirth[1].replaceFirst("0","");
            String year = dateOfBirth[2];
            selectDropDownByValue(daysDropDown, day);
            selectDropDownByValue(monthsDropDown, month);
            selectDropDownByValue(yearsDropDown, year);
            logger.log(LogStatus.PASS, "Successfully selected date of birth " + dOB);
        }catch (Exception e){
            logger.log(LogStatus.FAIL,"Failed to select date of birth "+dOB);
            throw e;
        }
    }
    protected String generateRandomEmail(String email) {
        String[] rEmail=email.split("@");
        int randomNum=(int)(Math.random()*1000);
        return rEmail[0]+randomNum+"@"+rEmail[1];
    }

    protected void login(String email,String pwd) throws Throwable {
        click(signInBtn,"SignIn");
        type(emailAddressTxt,email,"Email");
        type(passwordTxt,pwd,"password");
        click(submitloginBtn,"submitloginBtn");
    }

    protected void addItemTocart(String itemName) throws Throwable {

        waitForElementPresent(product);
        for (WebElement ele:getAllElements(product)) {
            if(ele.getText().equalsIgnoreCase(itemName)){
                ele.click();
                break;
            }
        }
    }

    protected void makePayment(String paymentMethod) throws Throwable {
        String paymentType=String.format(paymentMode,paymentMethod);
        waitForElementPresent(paymentType);
        click(paymentType,"Payment");
        waitForElementPresent(confirmMyOrderBtn);
        click(confirmMyOrderBtn,"Confirm order");
    }

    protected String getOrderReferenceNumber() throws Throwable {
        for (String s:getVisibleText(completeOrderDetails).split("\n")) {
            System.out.println(s.contains("Do not forget to insert your order reference"));
            if(s.contains("Do not forget to insert your order reference")){
                String[] temp1=s.split("reference");
                orderReferenceNumber=temp1[1].split(" ")[1];
                System.out.println("orderReferenceNumber===="+orderReferenceNumber);
                break;
            }
        }
        return orderReferenceNumber;
    }

    protected void verifyOrder(String orderRefeNumber) throws Throwable {
        ArrayList<String> orderList=new ArrayList<>();
        for (WebElement ele:getAllElements(orderedItemList)) {
            orderList.add(ele.getText());
        }
//        Assert.assertTrue(orderList.contains(orderRefeNumber));
        Assert.assertFalse(orderList.contains(orderRefeNumber));
    }

}
