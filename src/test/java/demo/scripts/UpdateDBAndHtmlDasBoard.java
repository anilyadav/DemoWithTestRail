//package demo.scripts;
//import com.ibatis.common.jdbc.ScriptRunner;
//import org.apache.commons.io.FileUtils;
//
//import java.io.*;
//import java.nio.charset.Charset;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.ResultSetMetaData;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//public class UpdateDBAndHtmlDasBoard {
//    static StringBuilder environmentSlideStrb=new StringBuilder();
//    static StringBuilder sparkDataStrb=new StringBuilder();
//    static String environmentSlide;
//    static String currentDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//    //    static String currentDate="2017-09-18";
//    static StringBuilder environmentSparkDataStrb=new StringBuilder();
//    static File sqlDumpFilepath=new File("C:\\Users\\E002465\\AtomBank\\hc.sql");
//
//    static ArrayList<Integer> allIress=new ArrayList<>();
//    static ArrayList<Integer> allDm=new ArrayList<>();
//    static ArrayList<Integer> allCc=new ArrayList<>();
//    static ArrayList<Integer> allCv=new ArrayList<>();
//    static ArrayList<Integer> allQas=new ArrayList<>();
//    static ArrayList<Integer> allBw=new ArrayList<>();
//    static ArrayList<Integer> allWp=new ArrayList<>();
//    static ArrayList<Integer> allCbv=new ArrayList<>();
//    static ArrayList<Integer> allLms=new ArrayList<>();
//    static ArrayList<Integer> allQust=new ArrayList<>();
//    static ArrayList<Integer> allWcsr=new ArrayList<>();
//    static ArrayList<Integer> allMastek=new ArrayList<>();
//    static ArrayList<Integer> allCommunicator=new ArrayList<>();
//    static ArrayList<Integer> allMa=new ArrayList<>();
//    static ArrayList<Integer> allIx=new ArrayList<>();
//    static ArrayList<Integer> allCscd=new ArrayList<>();
//    static ArrayList<Integer> allOb=new ArrayList<>();
//    static ArrayList<Integer> allPbus=new ArrayList<>();
//    static ArrayList<Integer> allSira=new ArrayList<>();
//    static ArrayList<Integer> allOrangeBus=new ArrayList<>();
//
//
//
//    public static void main(String[] args) throws SQLException {
//        Statement statement;
//
//        if (args.length == 0)
//            return;
//        if ((args.length % 2) != 0) {
//            System.out.println("Either a key or value is missing. Please verify your arguments");
//            return;
//        }
//        String dbuser = "";		//dashboard
//        String dbpassword = "";	//atom@123
//        String dbhost = "";		//172.31.25.7
//        int dbport = 0;			//3306
//        String dbname = "";		//test
//        String sql = "";
//        for(int i=0; i<args.length; i++) {
//            if(args[i].equalsIgnoreCase("-h")) {
//                //-h is for database hostip or hostname
//                dbhost = args[i + 1];
//            }
//            else if(args[i].equalsIgnoreCase("-p")) {
//                //-p is for database port
//                dbport = Integer.parseInt(args[i+1]);
//
//            }
//
//            else if(args[i].equalsIgnoreCase("-u")) {
//                //-u is for username
//                dbuser = args[i+1];
//
//            }
//
//            else if(args[i].equalsIgnoreCase("-pd")) {
//                //-pd is for DB password
//                dbpassword = args[i+1];
//
//            }
//
//            else if(args[i].equalsIgnoreCase("-db")) {
//                //-d is for destination folder path
//                dbname = args[i+1];
//
//            }
//        }
//
//        Connection connection = null;
//
//        try {
//            Class.forName("org.mariadb.jdbc.Driver");
//
//            System.out.println("Maridb JDBC Driver Registered!");
//            connection = DriverManager.getConnection(
//                    "jdbc:mariadb://"+dbhost+":"+dbport+"/"+dbname, dbuser, dbpassword);
//            System.out.println("Connected to database '"+ dbname +"' successfully...");
//
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        if (connection != null) {
//            System.out.println("You made it, take control your database now!");
//        } else {
//            System.out.println("Failed to make connection!");
//        }
//        try{
//
//            statement = connection.createStatement();
//            String environmentColumn = "SELECT environment FROM healthcheck where executed_date='"+currentDate+"';";
//            ResultSet ec = statement.executeQuery(environmentColumn);
//
//            ResultSetMetaData envData = ec.getMetaData();
//            int envColumns = envData.getColumnCount();
//            LinkedHashSet<String> enviList=new LinkedHashSet<>();
//            //Gets list of environments
//            while(ec.next()){
//                for (int i = 1; i <= envColumns; i++) {
//                    enviList.add(ec.getString(i));
//                }
//            }
//            //Inserts data into DB from specified sql file
//            insertValuesBySQL(connection,sqlDumpFilepath);
//            //Updates li tag for each environment
//            Iterator<String> itr = enviList.iterator();
//            while(itr.hasNext()){
//                String environment=itr.next().trim();
//                if(environment.equals("UAT")){
//                    updateLi(statement,environment);
//
//                }
//                else if(environment.equals("UAT2")){
//                    updateLi(statement,environment);
//                }
//                else if(environment.equals("SIT")){
//                    updateLi(statement,environment);
//                }
//            }
//
//            Iterator<String> itr1 = enviList.iterator();
//            //Update sparkdata for all environments
//            while(itr1.hasNext()){
//                String environment=itr1.next();
//                if(environment.equals("UAT")){
//                    environmentSparkDataStrb.append(updateEnviroment(statement,environment));
//                }
//                else if (environment.equals("UAT2")){
//                    environmentSparkDataStrb.append(updateEnviroment(statement,environment));
//                }
//                else if (environment.equals("SIT")){
//                    environmentSparkDataStrb.append(updateEnviroment(statement,environment));
//                }
//            }
//
//            File htmlTemplateFile = new File("C:\\Users\\E002465\\Desktop\\DemoFrame\\DemoFramework\\jq-dashboard.html");
//            String htmlString = FileUtils.readFileToString(htmlTemplateFile, Charset.forName("UTF-8"));
//            htmlString = htmlString.replace("$screens", environmentSlideStrb);
//            htmlString = htmlString.replace("$drawSparks", sparkDataStrb);
//            htmlString = htmlString.replace("$ForAllEnvironments", environmentSparkDataStrb);
//            File newHtmlFile = new File("C:\\Users\\E002465\\Desktop\\DemoFrame\\DemoFramework\\jq-dashboard1.html");
//            FileUtils.writeStringToFile(newHtmlFile, htmlString,Charset.forName("UTF-8"));
//        }
//        catch (SQLException e){
//            System.out.println("Failed");
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        connection.close();
//    }
//
//    private static void updateLi(Statement statement,String environment) throws SQLException, IOException {
//        int count=0;
//        StringBuilder CurrentDayTotalHtmlRowData=new StringBuilder();
//        String sparkData="";
//        int previousDataForSpecifiedNoOfDays=30;
//
//        //To get all rows of data for PresentDay
//        String queryToFetchCurrentDayResults = "SELECT * FROM healthcheck  where executed_date='"+currentDate+"' and environment='"+environment+"';";
//        ResultSet currentDayResultSet = statement.executeQuery(queryToFetchCurrentDayResults);
//        ResultSetMetaData currentDayMetadata = currentDayResultSet.getMetaData();
//        int currentDayColumns = currentDayMetadata.getColumnCount();
//        //Iterates over each row
//        while(currentDayResultSet.next()){
//            count++;
//            ArrayList<String> rowOfCurrentDay=new ArrayList<>();
//            //get data from each row
//            for (int i = 1; i <= currentDayColumns; i++) {
//                rowOfCurrentDay.add(currentDayResultSet.getString(i));
//            }
//            String tr = "<tr>\n" +
//                    "<td id=innerCell >"+count+"</td>\n" +
//                    "<td id=innerCell >"+rowOfCurrentDay.get(2)+"</td>\n" +
//                    "<td id=innerCell>"+rowOfCurrentDay.get(5)+"</td>\n" +
//                    "<td id=innerCell>"+rowOfCurrentDay.get(6)+"</td>\n" +
//                    "<td id=innerCell>"+rowOfCurrentDay.get(4)+"</td>\n" +
//                    "</tr>\n";
//            System.out.println(rowOfCurrentDay);
//            CurrentDayTotalHtmlRowData.append(tr);
//            rowOfCurrentDay.clear();
//        }
//        //forms the string consists of currentDay data
//        environmentSlide="<li><div>\n" +
//                "<table>\n" +
//                "<tbody>\n" +
//                "<tr>\n" +
//                "<td>\n" +
//                "<table id=innerTable style=\"margin-top:-85px;\">\n" +
//                "<caption>"+environment+"</caption>\n" +
//                "<thead>\n" +
//                "<tr>\n" +
//                "<th id=innerHeader scope=\"col\">Step No</th>\n" +
//                "<th id=innerHeader scope=\"col\">Application</th>\n" +
//                "<th id=innerHeader scope=\"col\">Status</th>\n" +
//                "<th id=innerHeader scope=\"col\">Conclusion</th>\n" +
//                "<th id=innerHeader scope=\"col\">Time-stamp</th>\n" +
//                "</tr>\n" +
//                "</thead>\n" +
//                "<tbody>\n" +
//                ""+CurrentDayTotalHtmlRowData+"\n" +
//                "\n" +
//                "</tbody>\n" +
//                "</table>\n" +
//                "</td>\n" +
//                "<td>\n" +
//                "<table id=innerTable style=\"margin-top:10px\">\n" +
//                "<tbody>\n" +
//                "<tr>\n" +
//                "<th id=innerHeader scope=\"col\">Application</th>\n" +
//                "<th id=innerHeader scope=\"col\" colspan=2>% Availability</th>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>IRESS</td>\n" +
//                "                            <td id=innerCell class='td_"+environment+"_IRESS'></td>\n" +
//                "                            <td id=innerCell><p><span class='"+environment+"_IRESS'>Loading..</span></p></td>\n" +
//                "                        </tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Decision Metrics</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Decision_Metrics'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Decision_Metrics'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Call Credit</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Call_Credit'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Call_Credit'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Call Validate</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Call_Validate'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Call_Validate'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Quick Address Search</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Quick_Address_Search'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Quick_Address_Search'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Bank Wizard</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Bank_Wizard'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Bank_Wizard'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>World Pay</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_World_Pay'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_World_Pay'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>CB Viewer</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_CB_Viewer'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_CB_Viewer'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>LMS</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_LMS'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_LMS'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Quest</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Quest'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Quest'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>WebCSR</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_WebCSR'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_WebCSR'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Mastek</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Mastek'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Mastek'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Communicator</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Communicator'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Communicator'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Mobile App</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Mobile_App'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Mobile_App'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Identity X</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Identity_X'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Identity_X'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>CSC Deon</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_CSC_Deon'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_CSC_Deon'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>On base</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_On_base'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_On_base'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>Phoebus</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_Phoebus'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_Phoebus'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>SIRA</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_SIRA'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_SIRA'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td id=innerCell>OrangeBus</td>\n" +
//                "<td id=innerCell class='td_"+environment+"_OrangeBus'></td>\n" +
//                "<td id=innerCell><p><span class='"+environment+"_OrangeBus'>Loading..</span></p></td>\n" +
//                "</tr>\n" +
//                "\n" +
//                "</tbody>\n" +
//                "</table>\n" +
//                "</td>\n" +
//                "</tr>\n" +
//                "<tr>\n" +
//                "<td>\n" +
//                "<table style=\"float: top;\">\n" +
//                "<tbody>\n" +
//                "<table id=innerTable style=\"margin-top:-70px;\">\n" +
//                "<thead>\n" +
//                "<tr>\n" +
//                "<th id=innerHeader scope=\"col\">Jira Issue Description</th>\n" +
//                "<th id=innerHeader scope=\"col\">Times repeated </th>\n" +
//                "</tr>\n" +
//                "</thead>\n" +
//                "</tbody>\n" + insertTopIssuesinDashBoard()+
//                "</tbody>\n" +
//                "</table>\n" +
//                "</td>\n" +
//                "</tr>\n" +
//                "</tbody>\n" +
//                "</table>\n" +
//                "</div>\n" +
//                "</li>\n";
//        environmentSlideStrb.append(environmentSlide);
//
//        // To get past results
//        String pastData = "SELECT * FROM healthcheck where environment='"+environment+"' ORDER  BY  executed_date DESC LIMIT "+(previousDataForSpecifiedNoOfDays*20)+";";
//        ResultSet pastResults = statement.executeQuery(pastData);
//
//        ResultSetMetaData pData = pastResults.getMetaData();
//        int columns1 = pData.getColumnCount();
//        //Iterates over each row
//        ArrayList<Integer> iress=new ArrayList<>();
//        ArrayList<Integer> dm=new ArrayList<>();
//        ArrayList<Integer> cc=new ArrayList<>();
//        ArrayList<Integer> cv=new ArrayList<>();
//        ArrayList<Integer> qas=new ArrayList<>();
//        ArrayList<Integer> bw=new ArrayList<>();
//        ArrayList<Integer> wp=new ArrayList<>();
//        ArrayList<Integer> cbv=new ArrayList<>();
//        ArrayList<Integer> lms=new ArrayList<>();
//        ArrayList<Integer> qust=new ArrayList<>();
//        ArrayList<Integer> wcsr=new ArrayList<>();
//        ArrayList<Integer> mastek=new ArrayList<>();
//        ArrayList<Integer> communicator=new ArrayList<>();
//        ArrayList<Integer> ma=new ArrayList<>();
//        ArrayList<Integer> ix=new ArrayList<>();
//        ArrayList<Integer> cscd=new ArrayList<>();
//        ArrayList<Integer> ob=new ArrayList<>();
//        ArrayList<Integer> pbus=new ArrayList<>();
//        ArrayList<Integer> sira=new ArrayList<>();
//        ArrayList<Integer> orangeBus=new ArrayList<>();
//        while(pastResults.next()){
//            count++;
//            ArrayList<String> al=new ArrayList<>();
//            //get data from each column per row
//            for (int i = 1; i <= columns1; i++) {
//                al.add(pastResults.getString(i));
//            }
//            switch (al.get(2).trim()){
//                case "IRESS":
//                    if(al.get(5).equals("Active")){
//                        iress.add(1);
//                    }else {
//                        iress.add(0);
//                    }
//                    break;
//                case "Decision Metrics":
//                    if(al.get(5).equals("Active")){
//                        dm.add(1);
//                    }else {
//                        dm.add(0);
//                    }
//                    break;
//                case "Call Credit":
//                    if(al.get(5).equals("Active")){
//                        cc.add(1);
//                    }else {
//                        cc.add(0);
//                    }
//                    break;
//                case "Call Validate":
//                    if(al.get(5).equals("Active")){
//                        cv.add(1);
//                    }else {
//                        cv.add(0);
//                    }
//                    break;
//                case "Quick Address Search":
//                    if(al.get(5).equals("Active")){
//                        qas.add(1);
//                    }else {
//                        qas.add(0);
//                    }
//                    break;
//                case "Bank Wizard":
//                    if(al.get(5).equals("Active")){
//                        bw.add(1);
//                    }else {
//                        bw.add(0);
//                    }
//                    break;
//                case "World Pay":
//                    if(al.get(5).equals("Active")){
//                        wp.add(1);
//                    }else {
//                        wp.add(0);
//                    }
//                    break;
//                case "CB Viewer":
//                    if(al.get(5).equals("Active")){
//                        cbv.add(1);
//                    }else {
//                        cbv.add(0);
//                    }
//                    break;
//                case "LMS":
//                    if(al.get(5).equals("Active")){
//                        lms.add(1);
//                    }else {
//                        lms.add(0);
//                    }
//                    break;
//                case "Quest":
//                    if(al.get(5).equals("Active")){
//                        qust.add(1);
//                    }else {
//                        qust.add(0);
//                    }
//                    break;
//                case "WebCSR":
//                    if(al.get(5).equals("Active")){
//                        wcsr.add(1);
//                    }else {
//                        wcsr.add(0);
//                    }
//                    break;
//                case "Mastek":
//                    if(al.get(5).equals("Active")){
//                        mastek.add(1);
//                    }else {
//                        mastek.add(0);
//                    }
//                    break;
//                case "Communicator":
//                    if(al.get(5).equals("Active")){
//                        communicator.add(1);
//                    }else {
//                        communicator.add(0);
//                    }
//                    break;
//                case "Mobile App":
//                    if(al.get(5).equals("Active")){
//                        ma.add(1);
//                    }else {
//                        ma.add(0);
//                    }
//                    break;
//                case "Identitiy X":
//                    if(al.get(5).equals("Active")){
//                        ix.add(1);
//                    }else {
//                        ix.add(0);
//                    }
//                    break;
//                case "CSC Deon":
//                    if(al.get(5).equals("Active")){
//                        cscd.add(1);
//                    }else {
//                        cscd.add(0);
//                    }
//                    break;
//                case "On base":
//                    if(al.get(5).equals("Active")){
//                        ob.add(1);
//                    }else {
//                        ob.add(0);
//                    }
//                    break;
//                case "Phoebus":
//                    if(al.get(5).equals("Active")){
//                        pbus.add(1);
//                    }else {
//                        pbus.add(0);
//                    }
//                    break;
//                case "SIRA":
//                    if(al.get(5).equals("Active")){
//                        sira.add(1);
//                    }else {
//                        sira.add(0);
//                    }
//                    break;
//                case "OrangeBus":
//                    if(al.get(5).equals("Active")){
//                        orangeBus.add(1);
//                    }else {
//                        orangeBus.add(0);
//                    }
//                    break;
//            }
//            al.clear();
//            sparkData="drawSparks($('."+environment+"_IRESS'), "+iress+");\n" +
//                    "drawSparks($('."+environment+"_Decision_Metrics'), "+dm+");\n" +
//                    "drawSparks($('."+environment+"_Call_Credit'), "+cc+");\n" +
//                    "drawSparks($('."+environment+"_Call_Validate'), "+cv+");\n" +
//                    "drawSparks($('."+environment+"_Quick_Address_Search'), "+qas+");\n" +
//                    "drawSparks($('."+environment+"_Bank_Wizard'), "+bw+");\n" +
//                    "drawSparks($('."+environment+"_World_Pay'), "+wp+");\n" +
//                    "drawSparks($('."+environment+"_CB_Viewer'), "+cbv+");\n" +
//                    "drawSparks($('."+environment+"_LMS'), "+lms+");\n" +
//                    "drawSparks($('."+environment+"_Quest'), "+qust+");\n" +
//                    "drawSparks($('."+environment+"_WebCsr'), "+wcsr+");\n" +
//                    "drawSparks($('."+environment+"_Mastek'), "+mastek+");\n" +
//                    "drawSparks($('."+environment+"_Communicator'), "+communicator+");\n" +
//                    "drawSparks($('."+environment+"_Mobile_App'), "+ma+");\n" +
//                    "drawSparks($('."+environment+"_Identity_X'), "+ix+");\n" +
//                    "drawSparks($('."+environment+"_CSC_Deon'), "+cscd+");\n" +
//                    "drawSparks($('."+environment+"_On_base'), "+ob+");\n" +
//                    "drawSparks($('."+environment+"_Phoebus'), "+pbus+");\n" +
//                    "drawSparks($('."+environment+"_SIRA'), "+sira+");\n" +
//                    "drawSparks($('."+environment+"_OrangeBus'), "+orangeBus+");";
//        }
//        sparkDataStrb.append(sparkData);
//    }
//    public static String updateEnviroment(Statement statement,String environment) throws SQLException, IOException{
//
//        String sparkDataenv = "";
//        String allenvironment = "AllEnv";
//        String uquery  = "select * From healthcheck where environment = '"+environment+"' AND executed_date > current_date - interval '7' day;";
//        ResultSet st = statement.executeQuery(uquery);
//        ResultSetMetaData data = st.getMetaData();
//        int columncount = data.getColumnCount();
//
//        while(st.next()){
//            ArrayList<String> al=new ArrayList<>();
//            //get data from each column per row
//            for (int i = 1; i <= columncount; i++) {
//                al.add(st.getString(i));
//            }
//            // System.out.println("al>>>>"+al);
//            switch (al.get(2).trim()){
//                case "IRESS":
//                    if(al.get(5).equals("Active")){
//                        allIress.add(1);
//                    }else {
//                        allIress.add(0);
//                    }
//                    break;
//                case "Decision Metrics":
//                    if(al.get(5).equals("Active")){
//                        allDm.add(1);
//                    }else {
//                        allDm.add(0);
//                    }
//                    break;
//                case "Call Credit":
//                    if(al.get(5).equals("Active")){
//                        allCc.add(1);
//                    }else {
//                        allCc.add(0);
//                    }
//                    break;
//                case "Call Validate":
//                    if(al.get(5).equals("Active")){
//                        allCv.add(1);
//                    }else {
//                        allCv.add(0);
//                    }
//                    break;
//                case "Quick Address Search":
//                    if(al.get(5).equals("Active")){
//                        allQas.add(1);
//                    }else {
//                        allQas.add(0);
//                    }
//                    break;
//                case "Bank Wizard":
//                    if(al.get(5).equals("Active")){
//                        allBw.add(1);
//                    }else {
//                        allBw.add(0);
//                    }
//                    break;
//                case "World Pay":
//                    if(al.get(5).equals("Active")){
//                        allWp.add(1);
//                    }else {
//                        allWp.add(0);
//                    }
//                    break;
//                case "CB Viewer":
//                    if(al.get(5).equals("Active")){
//                        allCbv.add(1);
//                    }else {
//                        allCbv.add(0);
//                    }
//                    break;
//                case "LMS":
//                    if(al.get(5).equals("Active")){
//                        allLms.add(1);
//                    }else {
//                        allLms.add(0);
//                    }
//                    break;
//                case "Quest":
//                    if(al.get(5).equals("Active")){
//                        allQust.add(1);
//                    }else {
//                        allQust.add(0);
//                    }
//                    break;
//                case "WebCSR":
//                    if(al.get(5).equals("Active")){
//                        allWcsr.add(1);
//                    }else {
//                        allWcsr.add(0);
//                    }
//                    break;
//                case "Mastek":
//                    if(al.get(5).equals("Active")){
//                        allMastek.add(1);
//                    }else {
//                        allMastek.add(0);
//                    }
//                    break;
//                case "Communicator":
//                    if(al.get(5).equals("Active")){
//                        allCommunicator.add(1);
//                    }else {
//                        allCommunicator.add(0);
//                    }
//                    break;
//                case "Mobile App":
//                    if(al.get(5).equals("Active")){
//                        allMa.add(1);
//                    }else {
//                        allMa.add(0);
//                    }
//                    break;
//                case "Identitiy X":
//                    if(al.get(5).equals("Active")){
//                        allIx.add(1);
//                    }else {
//                        allIx.add(0);
//                    }
//                    break;
//                case "CSC Deon":
//                    if(al.get(5).equals("Active")){
//                        allCscd.add(1);
//                    }else {
//                        allCscd.add(0);
//                    }
//                    break;
//                case "On base":
//                    if(al.get(5).equals("Active")){
//                        allOb.add(1);
//                    }else {
//                        allOb.add(0);
//                    }
//                    break;
//                case "Phoebus":
//                    if(al.get(5).equals("Active")){
//                        allPbus.add(1);
//                    }else {
//                        allPbus.add(0);
//                    }
//                    break;
//                case "SIRA":
//                    if(al.get(5).equals("Active")){
//                        allSira.add(1);
//                    }else {
//                        allSira.add(0);
//                    }
//                    break;
//                case "OrangeBus":
//                    if(al.get(5).equals("Active")){
//                        allOrangeBus.add(1);
//                    }else {
//                        allOrangeBus.add(0);
//                    }
//                    break;
//            }
//            al.clear();
//            sparkDataenv="drawSparks($('."+allenvironment+"_IRESS'), "+allIress+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Decision_Metrics'), "+allDm+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Call_Credit'), "+allCc+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Call_Validate'), "+allCv+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Quick_Address_Search'), "+allQas+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Bank_Wizard'), "+allBw+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_World_Pay'), "+allWp+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_CB_Viewer'), "+allCbv+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_LMS'), "+allLms+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Quest'), "+allQust+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_WebCsr'), "+allWcsr+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Mastek'), "+allMastek+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Communicator'), "+allCommunicator+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Mobile_App'), "+allMa+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Identity_X'), "+allIx+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_CSC_Deon'), "+allCscd+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_On_base'), "+allOb+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_Phoebus'), "+allPbus+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_SIRA'), "+allSira+");\n" +
//                    "\t\tdrawSparks($('."+allenvironment+"_OrangeBus'), "+allOrangeBus+");";
//
//
//        }
//        return sparkDataenv;
//
//    }
//    private static StringBuilder insertTopIssuesinDashBoard() throws IOException {
//        StringBuilder issueStrb=new StringBuilder();
//        int count=0;
//        Properties MyPropertyFile= new Properties();
//        String propertiesFilePath=System.getProperty("user.dir")+"/JiraIssues.properties";
//        FileInputStream fis = new FileInputStream(propertiesFilePath);
//        MyPropertyFile.load(fis);
//        LinkedHashMap<String, Integer> deffectMap=new LinkedHashMap<>();
//        Set<Object> keys = MyPropertyFile.keySet();
//
//        for(Object k:keys){
//            String key=(String) k;
//            int value=Integer.parseInt(MyPropertyFile.getProperty(key));
//            deffectMap.put(key, value);
//        }
//        if(deffectMap.size()>5){
//
//            for (Map.Entry<String,Integer> issues:deffectMap.entrySet()) {
//                if(count==5){
//                    break;
//                }
//                issueStrb.append("<tr>\n" +
//                        "<td id=innerCell> "+issues.getKey()+" </td>\n" +
//                        "<td id=innerCell> "+issues.getValue()+" </td>\n" +
//                        "</tr>\n" +
//                        "<tr>");
//
//                count++;
//            }
//        }else {
//            for (Map.Entry<String,Integer> issues:deffectMap.entrySet()) {
//                issueStrb.append("<tr>\n" +
//                        "<td id=innerCell> "+issues.getKey()+" </td>\n" +
//                        "<td id=innerCell> "+issues.getValue()+" </td>\n" +
//                        "</tr>\n" +
//                        "<tr>");
//            }
//        }
//        return issueStrb;
//    }
//
//
//    public static void insertValuesBySQL(Connection connection, File filePath){
//        try {
//
//            ScriptRunner sr = new ScriptRunner(connection, false, false);
//
//            // Give the input file to Reader
//            Reader reader = new BufferedReader(new FileReader(filePath));
////                    new FileReader(new File("C:\\Users\\E002465\\AtomBank\\hc.sql")));
//
//            // Exctute script
//            sr.runScript(reader);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }  catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//}
