package demo.scripts;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import org.apache.http.auth.AuthenticationException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.*;

import java.net.URLEncoder;
import java.util.*;

//import java.text.SimpleDateFormat;

public class JiraAPI {
    ArrayList<String> failedSummaryListFromCSV=new ArrayList<>();
    LinkedHashMap<String,String> summaryAndStatusFromJira=new LinkedHashMap<>();
    String auth= new String(Base64.encode("anil400.yadav@gmail.com" + ":" + "153695181"));
    ArrayList<String> incrementIssueCount=new ArrayList<>();
    Client clientQuery = Client.create();


    @Test
    public void jiraAPI()
    {

        String jsonFile = System.getProperty("user.dir")+File.separator+"config.json";
        try
        {
            failedSummaryListFromCSV.add("WebCSR-Unable to validate the customer details for case created in step 1 and OTP not Received edit");
            failedSummaryListFromCSV.add("This is Test Bug created through REST API 30");

            //adds the issue summary and status which are in status other than Done to "summaryAndStatusFromJira"
            getIssuesListFromJira();
            List<String> jiraIssuesSummary = new ArrayList<String>(summaryAndStatusFromJira.keySet());
            System.out.println(jiraIssuesSummary);
            //compares the summary of issues from Jira and csv file. If no match found creates an issue
            for (String temp:failedSummaryListFromCSV ){
                if(!jiraIssuesSummary.contains(temp)){
                    createDefectIfNotExist("HEAL",temp,"Bug");
                }else{
                    //summary of issue which is in still opne status will get added to "alreadyOpenedIssueSummary"
                    incrementIssueCount.add(temp);
                }
            }
            //Updates the issues occurance count to showcase in Dashboard
            updateIssueMap(incrementIssueCount);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * purpose: Gets the issues list from Jira
     * @throws AuthenticationException
     * @throws ClientHandlerException
     * @throws IOException
     */
    private void getIssuesListFromJira()throws AuthenticationException, ClientHandlerException, IOException{


        String jql = "status!=Done";
        String encodedJql = URLEncoder.encode(jql, "UTF-8");
        WebResource webResourceQuery = clientQuery.resource("https://abhealthcheck.atlassian.net/rest/api/2/search?jql="+encodedJql);
        ClientResponse responseQuery = webResourceQuery.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").get(ClientResponse.class);
        String jsonResponse = responseQuery.getEntity(String.class);
        //parse the Jira Response of Json file and gives summary of issues
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonResponse);
            JSONArray issues= (JSONArray) jsonObject.get("issues");
            Iterator i = issues.iterator();
            // take each value from the json array separately
            while (i.hasNext()) {
                JSONObject innerObj = (JSONObject) i.next();
//                System.out.println("-------->"+innerObj.get("fields"));
                JSONObject fields = (JSONObject)innerObj.get("fields");
                JSONObject fieldsStatus = (JSONObject)fields.get("status");
                summaryAndStatusFromJira.put(fields.get("summary").toString().trim(),fieldsStatus.get("name").toString());
            }
            System.out.println("summaryAndStatusFromJira  : "+summaryAndStatusFromJira);
        }  catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * purpose: Creates an  issue in Jira
     * @param projectKey
     * @param summary
     * @param issueType
     * @throws AuthenticationException
     * @throws ClientHandlerException
     * @throws IOException
     */

    private void createDefectIfNotExist(String projectKey,String summary,String issueType)throws AuthenticationException, ClientHandlerException, IOException{

        String encodedJql = URLEncoder.encode("text ~ \""+summary+"\"", "UTF-8");
        WebResource webResourceCreate = clientQuery.resource("https://abhealthcheck.atlassian.net/rest/api/2/search?jql="+encodedJql);
        ClientResponse responseCreate = webResourceCreate.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").get(ClientResponse.class);
        String jsonResponse = responseCreate.getEntity(String.class);
        //parse the Jira Response of Json file and gives summary of issues
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonResponse);
            int totalCount= Integer.parseInt(jsonObject.get("total").toString());
            if(totalCount==0){
                createDefect(projectKey,summary,issueType);
            }
            else if(totalCount!=0){
                JSONArray issues= (JSONArray) jsonObject.get("issues");
                if(issues.size()==1){
                    Iterator i = issues.iterator();
                    // take each value from the json array separately
                    while (i.hasNext()) {
                        JSONObject innerObj = (JSONObject) i.next();
                        updateDefect(innerObj.get("key").toString());
                    }
                }

            }
        }  catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * purpose: Creates an  issue in Jira
     * @param projectKey
     * @param summary
     * @param issueType
     * @throws AuthenticationException
     * @throws ClientHandlerException
     * @throws IOException
     */

    private void createDefect(String projectKey,String summary,String issueType)throws AuthenticationException, ClientHandlerException, IOException{

        WebResource webResourceCreate = clientQuery.resource("https://abhealthcheck.atlassian.net/rest/api/latest/issue");
        String data = "{\"fields\"" +":{\"project\"" + ":{\"key\":\""+projectKey+"\"},"+"\"summary\":\""+summary+" \",\"issuetype\":{\"name\":\""+issueType+"\"}}}";
        ClientResponse responseCreate = webResourceCreate.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").post(ClientResponse.class, data);
        int statusCode = responseCreate.getStatus();
        if (statusCode == 401) {
            throw new AuthenticationException("Invalid Username or Password");
        } else if (statusCode == 403) {
            throw new AuthenticationException("Forbidden");
        } else if (statusCode == 200 || statusCode == 201) {
            System.out.println("Ticket Create succesfully");
        } else if (statusCode==400) {
            System.out.println("Bad Request");
        } else {
            System.out.print("Http Error : " + statusCode);
        }
    }

    public  void updateIssueMap(ArrayList<String> alreadyOpenedIssueSummary) throws IOException {
        Properties MyPropertyFile= new Properties();
        String propertiesFilePath=System.getProperty("user.dir")+"/JiraIssues.properties";
        FileInputStream fis = new FileInputStream(propertiesFilePath);
        MyPropertyFile.load(fis);
        HashMap<String, Integer> deffectMap=new HashMap<>();
        HashMap<String, Integer> deffectMapWithNewEntry=new HashMap<>();
        Set<Object> keys = MyPropertyFile.keySet();

        for(Object k:keys){
            String key=(String) k;
            int value=Integer.parseInt(MyPropertyFile.getProperty(key));
            deffectMap.put(key, value);
        }

        for (String summary : alreadyOpenedIssueSummary) {
            boolean flag=false;
            Iterator<Map.Entry<String,Integer>>  it;
            Map.Entry<String,Integer>            entry;
            it = deffectMap.entrySet().iterator();
            while (it.hasNext())
            {
                entry = it.next();

                if (summary.replace(" ","_").equalsIgnoreCase(entry.getKey())) {
                    entry.setValue(entry.getValue()+1);
                    flag=true;
                    break;
                }
            }
            if(!flag) {
                deffectMapWithNewEntry.put(summary, 1);
            }
        }

        //sorting bases on values
        deffectMap.putAll(deffectMapWithNewEntry);
        Set<Map.Entry<String, Integer>> entries = deffectMap.entrySet();
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(entries);
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        //Replace the Unsorted Map with Sorted Map
        deffectMap.clear();
        for(Map.Entry<String, Integer> entry:list){
            deffectMap.put(entry.getKey(),entry.getValue());
        }



        //writes to .properties file
        try {
            BufferedWriter br1 = new BufferedWriter(new FileWriter(propertiesFilePath));
            {
                for (Map.Entry<String, Integer> entry1 : deffectMap.entrySet()) {
                    br1.write(entry1.getKey().replace(" ","_") + ":" + entry1.getValue());
                    br1.newLine();
                }
                br1.close();
            }
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
        }

    }
    private  void updateDefect(String key)throws AuthenticationException, ClientHandlerException, IOException{

        WebResource webResourceCreate = clientQuery.resource("https://abhealthcheck.atlassian.net/rest/api/2/issue/"+key+"/transitions?expand=transitions.fields");
        String data = "{\"transition\": {\"id\": \"11\"}}";
        ClientResponse responseCreate = webResourceCreate.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").post(ClientResponse.class, data);

    }
}
