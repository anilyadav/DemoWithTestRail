package demo.scripts;

import com.relevantcodes.extentreports.LogStatus;
import demo.businesslogic.CommonReusables_GCReddy;
import demo.objectrepository.OR_CreateAccount_GC_Reddy;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static utils.ExcelUtilsJxl.getTestData;

/**
 * Created by E002465 on 22-07-2017.
 */
public class AccountGCReddyCreation extends CommonReusables_GCReddy implements OR_CreateAccount_GC_Reddy {

    @Test(dataProvider = "testCases")
    public void createAccount(String testCaseID,String scenarioName,String gender,String firstName,String lastName,String dOB,String email,String comapnyName,String streetAddress,String suburb,String postCode,String city,String state,String country,String telephoneNumber,String faxNumber,String newsletter,String password,String passwordConfirmation) throws Throwable {
        logger=extent.startTest(scenarioName);
        /*click(myAccBtnBtn);
        Assert.assertEquals(getVisibleText(welcomeHeadre),"Welcome, Please Sign In");
        logger.log(LogStatus.PASS,"pass");
        click(createAccountContinueBtn);
        selectGender(gender);
        type(firstNameTxt,firstName);
        type(lasttNameTxt,lastName);
        type(dobTxt,dOB);
        type(emailAddressTxt,email);
        type(companyTxt,comapnyName);
        type(streetAddressTxt,streetAddress);
        type(postCodeTxt,postCode);
        type(cityTxt,city);
        type(stateTxt,state);
        selectDropDown(countryTxt,country);
        type(telephoneTxt,telephoneNumber);
        click(newsLetterTxt);
        type(passwordTxt,password);
        type(confirmPasswordTxt,passwordConfirmation);
        click(continueBtn);*/
        waitForElementPresent(accConfirmLabel,10);
        Assert.assertEquals(getVisibleText(accConfirmLabel),"Your Account Has Been Created!");
        Thread.sleep(10000);
    }

    @DataProvider
    public Object[][] testCases() throws Exception{

        Object[][] testObjArray = getTestData(System.getProperty("user.dir")+"\\TestData\\TestData.xls","TCTestData");

        return (testObjArray);
    }
}
