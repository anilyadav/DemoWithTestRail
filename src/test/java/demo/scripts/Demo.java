package demo.scripts;

import com.relevantcodes.extentreports.LogStatus;
import demo.businesslogic.CommonReusables;
import demo.objectrepository.OR_CreateAccount;
import demo.objectrepository.OR_OrderHistory;
import demo.objectrepository.OR_Signin;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static utils.ExcelUtilsJxl.getTestData;

/**
 * Created by E002465 on 01-08-2017.
 */
public class Demo extends CommonReusables implements OR_Signin,OR_CreateAccount,OR_OrderHistory {
    String randomEmail;


    @Test(dataProvider = "testDatacreateAccount")
    public void createAccount(String testCaseID,String scenarioName,String email,String gender,String firstName,String lastName,String password,String dOB,String a_FirstName,String a_LastName,String comapnyName,String address1,String city,String state,String postCode,String mobleNumber,String emailAlias
    ) throws Throwable {
        tcId = testCaseID;
        logger=extent.startTest("Create Account");
        click(signInBtn,"Signin");
        randomEmail=generateRandomEmail(email);
        userEmail=randomEmail;
        type(createAccEmailAddressTxt,randomEmail,"Create account Email Field");
        click(submitCreateBtn,"Create account submit button");
        selectGender(gender);
        type(firstNameTxt,firstName,"firstName");
        type(lastNameTxt,lastName,"lastName");
        type(passwdTxt,password,"password");
        pwd=password;
        selectDob(dOB);
        type(address_companyTxt,comapnyName,"comapnyName");
        type(address_address1Txt,address1,"address1");
        type(address_cityTxt,city,"city");
        selectDropDown(address_stateDropDown,state);
        type(address_postcodeTxt,postCode,"postCode");
        type(address_mobilePhoneTxt,mobleNumber,"mobleNumber");
        type(address_aliasTxt,emailAlias,"emailAlias");
        click(address_submitAccountBtn,"Account submit button");
        waitForElementPresent(logOutBtn,5);
        Assert.assertTrue(isElementPresent(logOutBtn));
        System.out.println(" email========="+userEmail);
        System.out.println(" password========="+pwd);
    }

    @Test(dataProvider = "testDataLoginAndAdditemToCart")
    public void loginAndAdditemToCart(String testCaseID, String itemName,String paymentType) throws Throwable {
        tcId = testCaseID;
        logger=extent.startTest("Login And Add item To Cart And MakePayment");
        login(userEmail,pwd);
        type(searchBarTxt,itemName,"Search bar");
        click(submitSearchBtn,"Search submit");
        addItemTocart(itemName);
        click(addTocartBtn,"AddToCart");
        waitForElementPresent(proceedToCheckOutBtn);
        click(proceedToCheckOutBtn,"proceedToCheckOutBtn");
        waitForElementPresent(proceedToCheckOutinSummaryBtn);
        click(proceedToCheckOutinSummaryBtn,"proceedToCheckOutinSummaryBtn");
        waitForElementPresent(proceedToCheckOutinAddressBtn);
        click(proceedToCheckOutinAddressBtn,"proceedToCheckOutinAddressBtn");
        waitForElementPresent(proceedToCheckOutinCarrierBtn);
        click(agreeToTAndCChkBox,"Agree to T&C");
        click(proceedToCheckOutinCarrierBtn,"proceedToCheckOutinCarrierBtn");
        makePayment(paymentType);
        Assert.assertEquals(getVisibleText(orderConfirmLabel),"Your order on My Store is complete.");
        getOrderReferenceNumber();
        logger.log(LogStatus.PASS,"Successfully placed Order");
    }

    @Test
    public void orderHistory() throws Throwable {

        tcId = "3";
        logger=extent.startTest("Verify order History");
        login(userEmail,pwd);
        click(accountName,"Account Name");
        waitForElementPresent(orderHistoryAndDetailsBtn);
        click(orderHistoryAndDetailsBtn,"Order history and Details");
        verifyOrder(orderReferenceNumber);

    }



    @DataProvider
    public Object[][] testDatacreateAccount() throws Exception{

        Object[][] testObjArray = getTestData(System.getProperty("user.dir")+"\\TestData\\TestDataAP.xls","CreateAccount");

        return (testObjArray);
    }

    @DataProvider
    public Object[][] testDataLoginAndAdditemToCart() throws Exception{

        Object[][] testObjArray = getTestData(System.getProperty("user.dir")+"\\TestData\\TestDataAP.xls","ItemSearchAndPayment");

        return (testObjArray);
    }

}
